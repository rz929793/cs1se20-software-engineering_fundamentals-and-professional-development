# CS1SE20 Software Engineering Seminar Document Collection

## Introduction
Welcome to the Seminar Document Collection Project! This project is an initiative to collect and organize the documents created by students during our seminars. It's a great way to showcase the work you've done and to share your ideas and designs with others.

## Purpose

The purpose of this project is to gather all the valuable documents designed by students in our seminars. This will not only help us keep a record of your creative work but also allow everyone to access and learn from each other's designs.

## How to Participate
Participating in this project is simple and straightforward. Follow these steps to contribute your documents:

### Commenting on Your Document
1. Log in your csgitlab and open this project [https://csgitlab.reading.ac.uk/rz929793/cs1se20-software-engineering_fundamentals-and-professional-development](https://csgitlab.reading.ac.uk/rz929793/cs1se20-software-engineering_fundamentals-and-professional-development)

1. Click "Last commit" of the designated file for the week in which the seminar took place to add a comment

1. Before uploading your document, please add a brief comment that should include:
    - The date of the seminar
    - A short description of the document
    - The names of the group members who worked on it

1. Uploading Your Document
Save your document with the following naming convention: **GroupName_Week**. For example, if your group is named "A1" and the seminar was on week 2, name your file **A1_week2**.

Upload the file to the designated file for the week in which the seminar took place. Make sure to do this within a week after the seminar.

### Weekly Submission Deadline
Please ensure that you upload your documents within a week of the seminar.

## Questions?
If you have any questions or need assistance, please feel free to reach out to the project coordinators.

Thank you for your participation and contributions to this project!
